package com.markash.bootbatis.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.markash.bootbatis.dao.UserMapper;
import com.markash.bootbatis.entity.UserEntity;
import com.markash.bootbatis.service.UserService;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;
    
    @Override
    public List<UserEntity> getAll() {
        UserEntity ue = userMapper.selectById(28L);
        userMapper.selectPage(new RowBounds(0, 10), 
                new EntityWrapper<UserEntity>());
        System.out.println(ue);
        return userMapper.getAll();
    }

}
