package com.markash.bootbatis.service;

import java.util.List;

import com.markash.bootbatis.entity.UserEntity;

public interface UserService {

    List<UserEntity> getAll();
}
