package com.markash.bootbatis.service;


import java.util.List;
import java.util.Map;

import com.markash.bootbatis.entity.SysLogEntity;

/**
 * 系统日志
 * @author Mark
 */
public interface SysLogService {
	
	SysLogEntity queryObject(Long id);
	
	List<SysLogEntity> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(SysLogEntity sysLog);
	
	void update(SysLogEntity sysLog);
	
	void delete(Long id);
	
	void deleteBatch(Long[] ids);
}
