package com.markash.bootbatis.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.markash.bootbatis.entity.UserEntity;
import com.markash.bootbatis.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@Api("用户基本操作")
public class UserController {

    @Autowired
    private UserService userService;
    
    @ApiOperation(value = "获取全部用户信息")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "用户ID"),
        @ApiImplicitParam(name = "name", value = "用户名称")
    })
    @GetMapping("/getUsers")
    public List<UserEntity> getUsers(@RequestParam String id, @RequestParam String name) {
        List<UserEntity> users = userService.getAll();
        return users;
    }
    
    @GetMapping("/getUsersByPage")
    public List<UserEntity> getUsersByPage(@RequestParam Map<String, Object> params) {
        List<UserEntity> users = userService.getAll();
        return users;
    }
}
