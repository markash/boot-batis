package com.markash.bootbatis.dao;


import org.apache.ibatis.annotations.Mapper;

import com.markash.bootbatis.entity.SysLogEntity;

/**
 * 系统日志
 * @author Mark
 */
@Mapper
public interface SysLogDao extends BaseDao<SysLogEntity> {
	
}
