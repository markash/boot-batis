package com.markash.bootbatis.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.markash.bootbatis.entity.UserEntity;

public interface UserMapper extends BaseMapper<UserEntity>{
    
    List<UserEntity> getAll();
}
