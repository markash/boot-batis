package com.markash.bootbatis.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("users")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId
    private Long id;
    
    @TableField("userName")
    private String userName;
    
    @TableField("passWord")
    private String passWord;
    
    @TableField("nick_Name")
    private String nickName;

    public UserEntity() {
        super();
    }

    public UserEntity(String userName, String passWord) {
        super();
        this.passWord = passWord;
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "userName " + this.userName + ", pasword " + this.passWord;
    }
}
